-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Jeu 05 Mars 2015 à 21:58
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `marathonweb`
--
CREATE DATABASE IF NOT EXISTS `cerf3u` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;


-- --------------------------------------------------------

--
-- Structure de la table `ccd_items`
--

CREATE TABLE IF NOT EXISTS `ccd_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `piece_id` int(11) NOT NULL,
  `photo` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `couleur` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `prix` float NOT NULL,
  `aime` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Contenu de la table `ccd_items`
--

INSERT INTO `ccd_items` (`id`, `nom`, `description`, `piece_id`, `photo`, `couleur`, `prix`, `aime`, `type_id`, `created_at`, `updated_at`) VALUES
(1, 'Fauteuil gonflable ', 'Un joli fauteuil gonflable', 1, 'fauteuil_gonflable.jpg', 'Mauve', 150, 4, 4, '0000-00-00', '2015-03-05'),
(2, 'Sofa Gonflable', 'Un joli sofa gonflable', 1, 'sofa_gonflable.jpg', 'Orange', 175.99, 2, 4, '0000-00-00', '2015-03-05'),
(3, 'Fauteuil en carton', 'Un joli fauteuil en carton', 1, 'fauteuil.jpg', 'Maron', 15.95, 0, 1, '0000-00-00', NULL),
(4, 'Vache', 'Un vache, ni plus, ni moins', 2, 'vache_blanche.jpg', 'Blanche', 9.99, 0, 1, '0000-00-00', NULL),
(5, 'Fauteuil en palettes', 'Un fauteuil 100% eco', 2, 'meuble_palette.jpg', 'Marron', 230.9, 0, 2, '0000-00-00', NULL),
(6, 'Tortue', 'Une tortue', 3, 'tortue_rouge.jpg', 'Rouge', 25.85, 0, 1, '0000-00-00', NULL),
(7, 'Tortue', 'Une autre tortue', 3, 'tortue_brun.jpg', 'Brun', 25.85, 0, 1, '0000-00-00', NULL),
(8, 'Canard Gonflable', 'Un canard qui n est pas dans sa flaque', 4, 'canard_gonflable.jpg', 'Jaune', 4.99, 22, 4, '0000-00-00', '2015-03-05');

-- --------------------------------------------------------

--
-- Structure de la table `ccd_pieces`
--

CREATE TABLE IF NOT EXISTS `ccd_pieces` (
  `id` int(11) NOT NULL,
  `nom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `ccd_pieces`
--

INSERT INTO `ccd_pieces` (`id`, `nom`, `description`) VALUES
(1, 'Salon', 'Là où on se repose'),
(2, 'Salle à manger', 'Là où on mange'),
(3, 'Chambre à coucher', 'Là où on fait dodo'),
(4, 'Salle de bain', 'J''en ressort tout propre');

-- --------------------------------------------------------

--
-- Structure de la table `ccd_top`
--

CREATE TABLE IF NOT EXISTS `ccd_top` (
  `item_id` int(11) NOT NULL,
  `description` text,
  PRIMARY KEY (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ccd_top`
--

INSERT INTO `ccd_top` (`item_id`, `description`) VALUES
(5, 'Promotion stratosphèrique'),
(6, 'Promotion exeptionnelle'),
(7, 'Promotion incroyable');

-- --------------------------------------------------------

--
-- Structure de la table `ccd_types`
--

CREATE TABLE IF NOT EXISTS `ccd_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `ccd_types`
--

INSERT INTO `ccd_types` (`id`, `type`) VALUES
(1, 'Carton'),
(2, 'Récupération'),
(3, 'Recyclage'),
(4, 'Gonflable');

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `level` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `role`
--

INSERT INTO `role` (`id`, `name`, `level`) VALUES
(1, 'membre', 1),
(2, 'admin', 2);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `name`, `password`, `role`) VALUES
(1, 'Administrateur', '$2y$12$Y4uxDORx3Wun3/GV3TbjUOZYE1VyBoZlWcmx6RoY4mo4l28lJSTmi', 2),
(3, 'Baptiste', '$2y$12$qASmmhH3FvPUL86DLaGckuPbZW88RG5VlJDShmFac2QcXlq2./79m', 1),
(4, 'Joris', '$2y$12$At.WJRRApohW0bJDW2b8n.Iu0cmy04E..E4m7K1xxzmmMEp7yDum2', 1),
(5, 'Pierre', '$2y$12$FrmKvEFqy9DqugdI7/iVHeXBtOGbgyjhahA4x/0ga/T6nmOO1amx6', 1),
(6, 'Paul', '$2y$12$xrnZ7XeJeGtcaf.f8SYsBeMS3MHrtALP9OEiWvQGqtUqouvWPEOiC', 1),
(7, 'Jacque', '$2y$12$wklyV3lpHI5WMjcH49DVPuHQc.IQdj908iSOUngQqdfdwC/OpAgL6', 1);