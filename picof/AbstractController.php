<?php

	namespace picof;

    abstract class AbstractController{
        protected $http;
        public function __construct(\picof\utils\HttpRequest $http) {
			$this->http = $http;
        }
        
        function __get($attname){
            if(property_exists($this, $attname)){
                return $this->$attname;
            }else{
                throw new \Exception("get : attribut inconnu - $attname");
            }
        }
        
        function __set($attname, $attval){
            if(property_exists($this, $attname)){
                $this->$attname = $attval;
                return $this->$attname;
            }else{
                throw new \Exception("set : attribut inconnu - $attname");
            }
        }
        
    }
