<?php

	namespace picof\dispatch;
	use \picof\utils\HttpRequest;

    class Dispatcher{
        
        protected $http,$listeRoute = array();
        
        public function __construct(HttpRequest $h) {
			$this->http = $h;
		}
        
        function __get($attname){
            if(property_exists($this, $attname)){
                return $this->$attname;
            }else{
                throw new \Exception("get : attribut inconnu - $attname");
            }
        }
        
        function __set($attname, $attval){
            if(property_exists($this, $attname)){
                $this->$attname = $attval;
                return $this->$attname;
            }else{
                throw new \Exception("set : attribut inconnu - $attname");
            }
        }
		
		public function addRoute($uri,$controllerName,$methodName){
			$this->listeRoute[$uri] = array("controllerName" => $controllerName, "methodName" => $methodName);
		}
		
		public function dispatch(){
			$r = $this->http->getPathInfo();
            $route = explode('?', $r);
            $t = $this->listeRoute[$route[0]];
            $classeName = $t['controllerName'];
            $classe = new $classeName($this->http);
            $methodName = $t['methodName'];
            $classe->$methodName();
				
			
		}
        
    }
