<?php

	namespace picof\utils;

    class HttpRequest{
        
        protected $method,$script_name,$request_uri, $query,$get;
        
        public function __construct() {
			$this->method = $_SERVER['REQUEST_METHOD'];
			$this->script_name = $_SERVER['SCRIPT_NAME'];
			$this->request_uri = $_SERVER['REQUEST_URI'];
			$this->query = $_SERVER['QUERY_STRING'];
			$this->get = $_GET;
        }
        
        function __get($attname){
            if(property_exists($this, $attname)){
                return $this->$attname;
            }else{
                throw new \Exception("get : attribut inconnu - $attname");
            }
        }
        
        function __set($attname, $attval){
            if(property_exists($this, $attname)){
                $this->$attname = $attval;
                return $this->$attname;
            }else{
                throw new \Exception("set : attribut inconnu - $attname");
            }
        }
		
		function getPathInfo(){
			$pi = dirname($_SERVER['SCRIPT_NAME']);
			$pi = str_replace($pi,"",$_SERVER['REQUEST_URI']);
			return $pi;
		}
		
		 public function getRoute(){
			return (dirname($this->script_name));
		}
        
    }
