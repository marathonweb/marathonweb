<?php

	namespace picof\utils;
	use \picof\AbstractController;
    use \app\model\Item;
    use \app\model\Piece;
    use \app\model\Type;
    use \app\model\Top;
    use \app\model\User;
    use \app\model\Role;
    use \app\vue\Vue;

    class Authentification {
  
        // réaliser l'authentifcation : contrôler identifant et mot de passe        
        public static function authenticate($username,$pass){
            session_unset();
            $b = false;
            $u = User::where('name','=',$username)->get();
            $hash = $u[0]->password;
            if(password_verify($pass,$hash))
                $b = true;
            return $b;
        }
        
        // charger le profl d'un utlisateur en session
        public static function loadProfile($uid){
            session_unset();
            $us = User::find($uid);
            $rl_id = $us->role;
            $rl = Role::find($rl_id);
            $_SESSION['username'] = $us->name;
            $_SESSION['userid'] = $us->id;
            $_SESSION['roleid'] = $us->role;
            //$_SESSION['client_ip'] = ;
            $_SESSION['auth-level'] = $rl->level;    
        }
        
        // comparer required et le contenu de la variable de session stockant le profl de l'utilisateur courant
        public static function checkAccessRights($required){
            $right = false;
            $lvl = $_SESSION['auth-level'];
            if($required <= $lvl)
                $right = true;
            return $right;
        }
        
        // créer un utilisateur et défnir ses droits
        public static function createUser($username,$pass){
            session_unset();
            $new = new User();
            $new->name = $username;
            $hash = password_hash($pass,PASSWORD_DEFAULT,array('cost'=>12));
            $new->password = $hash;
            $new->role = 1;
            $new->save();
            Authentification::loadProfile($new->id);
        }
        
        // déconnection
        public static function disconnect(){
            session_unset();
            session_destroy();
        }
        
    }