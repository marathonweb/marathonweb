Notre groupe est composé de : **Anthony Cerf**, **Romain Ruez**, **Joris Vigneron** et  **Baptiste Mounier**, classe de **SI-1**.

Nous avons essayé de développer une **application web** représentant le catalogue d'une entreprise de meubles gonflables et/ou en carton et/ou en matériaux de récupération souhaite s’implanter dans l’agglomération nancéienne.

Pour répondre aux besoin du client, nous avons élaboré les fonctionnalités suivantes :
                                    
                                 1. Afficher la liste des items
                                 2. Afficher le détail d'un item avec toute les informations necessaire dans une fenetre modale
                                 3. Clique quand la liste permet d'afficher le détail de l'item
                                 4. filtrage par type, couleur et pièce : Il est seulement possible de faire un choix par type de filtre mais vous avez la possibilité
                                    de choisir plusieurs filtres
                                 5. identification de l'administrateur
                                 6. contrôle d'accès : il est possible de se connecter si l'on dispose des droits d'utilisateurs.
                       Sur  la feuille des fonctionnalités ( 1 , 2 , 3 , 4 , 5
                       , 6 , 7 , 15 , 16 ) 
                                  possibilité de mettre des jaime seulement en étant connecté



Pour tester notre application, vous pouvez vous rendre sur le lien suivant :
https://webetu.iutnc.univ-lorraine.fr/www/cerf3u/marathonweb/

Pour vous connecter en tant qu'administrateur : login : Baptiste, mdp : baptiste

Le fichiers des bases de données se trouve dans le fichier : ccd.sql