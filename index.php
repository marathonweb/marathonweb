<?php
require_once 'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

use \arf\ConnectionFactory;
use \arf\AbstractModel;

use \conf\confEloquent;

use \picof\utils\HttpRequest;
use \picof\dispatch\Dispatcher;

confEloquent::connect('.'.DIRECTORY_SEPARATOR.'conf'.DIRECTORY_SEPARATOR.'db.config.ini');

$http = new HttpRequest();
$disp = new Dispatcher($http);

$disp->addRoute('/','\app\control\Controller','accueil');
$disp->addRoute('/accueil','\app\control\Controller','accueil');
$disp->addRoute('/test','\app\control\Controller','test');
$disp->addRoute('/items','\app\control\Controller','afficherItem');
$disp->addRoute('/filtre','\app\control\Controller','filtre');
$disp->addRoute('/connexion','\app\control\Controller','connexion');
$disp->addRoute('/piece','\app\control\Controller','afficherPiece');
$disp->addRoute('/inscription','\app\control\Controller','inscription');
$disp->addRoute('/profil','\app\control\Controller','profil');
$disp->addRoute('/deconnexion','\app\control\Controller','deconnexion');

$disp->addRoute('/aime','\app\control\Controller','aime');
$disp->dispatch();