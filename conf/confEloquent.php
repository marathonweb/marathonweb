<?php

namespace conf;
use Illuminate\Database\Capsule\Manager as DB;

    class confEloquent {
        
        public static function connect($filename){
            
            $ini=parse_ini_file($filename);
            
            $db = new DB();
            $db->addConnection($ini) ;
            $db->setAsGlobal();
            $db->bootEloquent();
        }
        
    }