<?php

    namespace app\model;

    class User extends \Illuminate\Database\Eloquent\Model{
        protected $table = 'user';
        protected $primary_key = 'id';
        public $timestamps = false;
        
        public function role(){
            return $this->belongsTo('\app\model\Role','role');
        }
        
    }