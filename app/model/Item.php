<?php

    namespace app\model;

    class Item extends \Illuminate\Database\Eloquent\Model{
        protected $table = 'ccd_items';
        protected $primary_key = 'id';
        public $timestamps = true;
        
        public function piece(){
            return $this->belongsTo('\app\model\Piece','piece_id');
        }
        
        public function type(){
            return $this->belongsTo('\app\model\Role','type_id');
        }
        
        public function top(){
            return $this->hasMany('\app\model\Item','item_id');
        }
        
    }