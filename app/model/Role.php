<?php

    namespace app\model;

    class Role extends \Illuminate\Database\Eloquent\Model{
        protected $table = 'role';
        protected $primary_key = 'id' ;
        public $timestamps = false;
        
        public function users(){
            return $this->hasMany('\app\model\User','role');
        }
        
    }