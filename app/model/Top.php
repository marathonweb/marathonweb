<?php

    namespace app\model;

    class Top extends \Illuminate\Database\Eloquent\Model{
        protected $table = 'ccd_item';
        protected $primary_key = 'id';
        public $timestamps = false;
        
        public function piece(){
            return $this->belongsTo('\app\model\Item','id');
        }
        
    }