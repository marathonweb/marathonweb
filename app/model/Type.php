<?php

    namespace app\model;

    class Type extends \Illuminate\Database\Eloquent\Model{
        protected $table = 'ccd_types';
        protected $primary_key = 'id' ;
        public $timestamps = false;
        
        public function items(){
            return $this->hasMany('\app\model\Item','type_id');
        }
        
    }