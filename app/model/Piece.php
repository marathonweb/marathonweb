<?php

    namespace app\model;

    class Piece extends \Illuminate\Database\Eloquent\Model{
        protected $table = 'ccd_pieces';
        protected $primary_key = 'id' ;
        public $timestamps = false;
        
        public function items(){
            return $this->hasMany('\app\model\Item','piece_id');
        }
        
    }