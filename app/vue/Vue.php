<?php
namespace app\vue;
/*use \app\model\Billets;
use \app\model\Categorie;
use \app\model\User;
use \app\vue\VueBlog;*/

use picof\utils\HttpRequest;

class Vue {

    protected $HttpRequest;
    public $listeitem;
    public $listepiece;
    public $listetype;
    public $piece;


    public function __construct(){
        $this->HttpRequest = new HttpRequest();
        $listeitem = "";
        $listepiece = "";
        $listetype ="";
    }
    
    public function render($mode){
        $page ='    <!DOCTYPE html>
<html>
<head>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	       <title>Responsive Web Mobile - Ecommerce</title>

	       <!-- Included Bootstrap CSS Files -->
	       <link rel="stylesheet" href="'. $this->HttpRequest->getRoute().'/web/js/bootstrap/css/bootstrap.min.css" />
	       <link rel="stylesheet" href="'.$this->HttpRequest->getRoute().'/web/js/bootstrap/css/bootstrap-responsive.min.css" />

	       <!-- Includes FontAwesome -->
	       <link rel="stylesheet" href="'.$this->HttpRequest->getRoute().'/web/css/font-awesome/css/font-awesome.min.css" />

	       <!-- Css -->
	           <link rel="stylesheet" href="'.$this->HttpRequest->getRoute().'/web/css/style.css" />

</head>
<body>';
        switch($mode){
            // affichage accueil site
            case 1 :
                $page .= $this->creerPageAccueil();
                break;
            case 3 :
                $page .= $this->creerPageCatalogue();
                $page .= $this->afficherCat();
                break;
            case 5 :
                $page .= $this->afficherPiece();
                break;
            case 9 :
                $page .= '<div class="navbar navbar-inverse navbar-fixed-top">
		    <div class="navbar-inner">
			<div class="container">
				<button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
                <ul class="nav">
						<li class="dropdown">
							<a href="'.$this->HttpRequest->getRoute().'/items">Catalogue</a>
                            
						</li>
                        <li>
                        <a href="'.$this->HttpRequest->getRoute().'/connexion">Connexion</a>
                        </li>
                        <li>
                        <a href="'.$this->HttpRequest->getRoute().'/inscription">Connexion</a>
                        </li>
					</ul>
			</div>
		  </div><h1>Erreur ! Vous êtes déjà connecté !</h1></body>'; 
                break;
            case 8 :
                $page .= $this->forConnexion();
                break;
            case 10 :
                $page .= $this->forInscription();
                break;
            case 11 :
                
        }

        $page = $page .'<footer id="footer" class="vspace20">

    </footer>	 
    <script src="'.$this->HttpRequest->getRoute().'/web/js/jquery-1.10.0.min.js"></script>
	<script src="'.$this->HttpRequest->getRoute().'/web/js/bootstrap/js/bootstrap.min.js"></script>
	<script src="'.$this->HttpRequest->getRoute().'/web/js/holder.js"></script>
	<script src="'.$this->HttpRequest->getRoute().'/web/js/script.js"></script>
	<script src="'.$this->HttpRequest->getRoute().'/web/js/js_popup.js"></script>
	<script src="'.$this->HttpRequest->getRoute().'/web/js/bootstrap/js/bootstrap.js"></script>



    </html>';

        echo $page;

    }
    
    private function afficherPiece(){
        $page ='<div class="navbar navbar-inverse navbar-fixed-top">
		    <div class="navbar-inner">
			<div class="container">
				<button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
                <ul class="nav">
						<li class="dropdown">
							<a href="'.$this->HttpRequest->getRoute().'/items">Catalogue</a>
                            
						</li>
                        <li>
                        <a href="'.$this->HttpRequest->getRoute().'/connexion">Connexion</a>
                        </li>
                        <li>
                        <a href="'.$this->HttpRequest->getRoute().'/inscription">Inscription</a>
                        </li>
					</ul>
			</div>
		  </div>
          <div>
            <h1>'.$this->piece->nom.'</h1>
            <p>'.$this->piece->description.'</p>
            <ul>';
        foreach($this->listeitem as $item){
               if($item->piece_id == $this->piece->id)
                   $page .= '<li><p>'.$item->nom.'</p>';
        }
        
        $page .=  '</ul></div>';
    }
    
    private function forInscription(){
        $page = '<div class="navbar navbar-inverse navbar-fixed-top">
		    <div class="navbar-inner">
			<div class="container">
				<button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
                <ul class="nav">
						<li class="dropdown">
							<a href="'.$this->HttpRequest->getRoute().'/items">Catalogue</a>
                            
						</li>
                        <li>
                        <a href="'.$this->HttpRequest->getRoute().'/connexion">Connexion</a>
                        </li>
                        <li>
                        <a href="'.$this->HttpRequest->getRoute().'/inscription">Inscription</a>
                        </li>
					</ul>
			</div>
		  </div>
        <div class="co">  
        <h2 class="ht">Inscription</h2>
        <p>Fiche inscription</p>
        <div>
        <form id="f1" method="post" action="profil">
        <p>
        <label for="pseudo"> Username : </label>
        <input type="text" name="username" required>
        </p>
        <p>
        <label for="password">Password :</label>
        <input type="password" name="password">
        </p>
        <p>
        <button type="submit" name="valider">Valider</button>
        </p>
        </form>
        </div>
        </div>
	       </div>
 
        
        
        
        
        ';
        return $page;
        
    }

    private function forConnexion(){
        $page = '<div class="navbar navbar-inverse navbar-fixed-top">
		    <div class="navbar-inner">
			<div class="container">
				<button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
                <ul class="nav">
						<li class="dropdown">
							<a href="'.$this->HttpRequest->getRoute().'/items">Catalogue</a>
                            
						</li>
                        <li>
                        <a href="'.$this->HttpRequest->getRoute().'/connexion">Connexion</a>
                        </li>
                        <li>
                        <a href="'.$this->HttpRequest->getRoute().'/inscription">Inscription</a>
                        </li>
					</ul>
			</div>
		  </div>
        <div class="co">  
        <h2 class="ht">Connexion</h2>
        <p>Veuillez vous authentifier</p>
        <div>
        <form id="f1" method="post" action="profil">
        <p>
        <label for="pseudo"> Username : </label>
        <input type="text" name="username" required>
        </p>
        <p>
        <label for="password">Password :</label>
        <input type="password" name="password">
        </p>
        <p>
        <button type="submit" name="valider">Valider</button>
        </p>
        </form>
        </div>
        </div>
	       </div>
 
        
        
        
        
        ';
        return $page;
    }
    
    private function afficherCat(){

        $page= '<ul class="thumbnails"><form id="f1" method="post" action="aime">';

        $p = "";
        $t = "";

        foreach($this->listeitem as $item){
            foreach($this->listepiece as $piece){

                if($item->id_piece == $piece->id)
                    $p = $piece->nom  ;
            }
            
            foreach($this->listetype as $type){
                if($item->type_id == $type->id)
                    $t = $type->type;
            }
            $page .= '
                    <li class="span3">
						<div class="thumbnail">
							<img class="img" src="'.$this->HttpRequest->getRoute().'/web/image/'. $item->photo.'" height ="400" width="400">
							<div class="caption">
								<h4>'.$item->nom.'</h4>
								<p>'.round($item->prix,2).' €</p>
							      <a class="btn btn-primary" href="#'.$item->id.'">Détails</a>
                                <button type="submit" value="'.$item->id.'" name="id">+1</button>
                                <label for="jaime">Aime :'.$item->aime.'</label>
							</div>
						</div>
						<div id="'.$item->id.'" class="modalDialog">
	                 <div>
		                 <a href="'.$this->HttpRequest->getRoute().'/items" title="Close" ">Fermer</a>
		                 <h1>'.$item->nom.'</h1>
		                  <img class="img2" src="'.$this->HttpRequest->getRoute().'/web/image/'. $item->photo.'">
			              <p>'.$item->description.'</p>

			              <p>'.round($item->prix,2).'</p>
			               <p>'.$item->couleur.'</p>
			               <p>'.$t.'</p>
			               <p>'.$p.'</p>

	                  </div>
                     </div>
					</li>';



        }
        $page .= '

			</div>
		</div>
	</div></ul></form>';


        return $page;
    }
    public function setPiece($p){
        $this->piece = $p;
    }


    public function setListeItem($list){
        $this->listeitem = $list;
    }

    public function setPieces($list){
        $this->listepiece= $list;
    }

    public function setTypes($list){
        $this->listetype = $list;
    }

    private function creerPageAccueil(){

        $page = '
    

<div class="navbar navbar-inverse navbar-fixed-top">
		    <div class="navbar-inner">
			<div class="container">
				<button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
                <ul class="nav">
						<li class="dropdown">
							<a href="'.$this->HttpRequest->getRoute().'/items">Catalogue</a>
                            
						</li>
                        <li>
                        <a href="'.$this->HttpRequest->getRoute().'/connexion">Connexion</a>
                        </li>
                        <li>
                        <a href="'.$this->HttpRequest->getRoute().'/inscription">Inscription</a>
                        </li>
					</ul>
			</div>
		  </div>

	       </div>
            <div class="container">
		      <div class="hero-unit">'
            .'<h1 class="">Offre spéciale</h1>'
            .'<p class="">Un nouveau magasin de meubles innovants débarque en ville ! Venez découvrir ces créations impessionnantes et pleines de styles. Des promotions très attractives vous attendent alors venez vite !</p>'


            .'</div>  
            <form  class="forAcc" id="f1" method="post" enctype="multipart/form-data" action="items">
            <button class="bout1" type="submit" class="btn">Vers le catalogue</button>
            </form>'.
            /*<div class ="horizontal">

            <img class = "imgAcc" src="'.$this->HttpRequest->getRoute().'/web/image/tortue_rouge.jpg"/>
            <img class = "imgAcc" src="'.$this->HttpRequest->getRoute().'/web/image/tortue_brun.jpg" />
            <img class = "imgAcc" src="'.$this->HttpRequest->getRoute().'/web/image/canard_gonflable.jpg" />
            </div>*/

            '</div>';

        return $page;

    }



    private function creerPageCatalogue(){
        $page ='
            <!DOCTYPE html>
            <html lang="">
            <head>
	       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
	       <title>Responsive Web Mobile - Ecommerce</title>

	       <!-- Included Bootstrap CSS Files -->
	       <link rel="stylesheet" href="'. $this->HttpRequest->getRoute().'/web/js/bootstrap/css/bootstrap.min.css" />
	       <link rel="stylesheet" href="'.$this->HttpRequest->getRoute().'/web/js/bootstrap/css/bootstrap-responsive.min.css" />
	
	       <!-- Includes FontAwesome -->
	       <link rel="stylesheet" href="'.$this->HttpRequest->getRoute().'/web/css/font-awesome/css/font-awesome.min.css" />

	       <!-- Css -->	
	           <link rel="stylesheet" href="'.$this->HttpRequest->getRoute().'/web/css/style.css" />

            </head>

            <body>
            <div class="navbar navbar-inverse navbar-fixed-top">
		    <div class="navbar-inner">
			<div class="container">
				<button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<ul class="nav">
						<li class="dropdown">
							<a href="'.$this->HttpRequest->getRoute().'/items">Catalogue</a>
                            
						</li>
                        <li class="dropdown">
                        <a href="'.$this->HttpRequest->getRoute().'/connexion">Connexion</a>
                        </li>
                        <li>
                        <a href="'.$this->HttpRequest->getRoute().'/inscription">Inscription</a>
                        </li>
					</ul>
				<div class="nav-collapse collapse">
					
					
				</div>
			</div>
		  </div>';
        $page .= $this->creerSide();



        $page .= '
                <div class="well">
					<h4>Filters</h4>
                    <form id="f1" method="post" enctype="multipart/form-data" action="filtre">
                    <label for="piece">Piece:</label>
                    <select name="piece" size="1">
                    <option>-</option>';
                    foreach($this->listepiece as $piece){
                            $page .= '<option value = '.$piece->id.'>'.$piece->nom.'</option>';

                    }


        $page .= '</select><label for="type">Type:</label>
                    <select name="type" size="1">
                    <option>-</option>';
        foreach($this->listetype as $type){
            $page .= '<option value = '.$type->id.'>'.$type->type.'</option>';

        }


        $page.='</select><label for="couleur">Couleur:</label>
                    <select name="couleur" size="1">
                        <option>-</option>
                        <option>Mauve</option>
                        <option>Orange</option>
                        <option>Marron</option>
                        <option>Blanche</option>
                        <option>Rouge</option>
                        <option>Jaune</option>
                        <option>Brun</option>
                       </select>
					   <button class="btn btn-primary pull-right" type="submit" >Filtrer</button>
                       
					</form>
                    
                    
                    
                    
				</div><div class="well">
					
				</div>

				<div class="well">
					
				</div>
			</div>
            <div class="span9">' ;
        return $page;
    }


    private function creerSide(){
        $page = '</div><div class="container">
		      <div class="row">
			     <div class="span3">
                    <div class="well">
					   <ul class="nav nav-list">
                       <li class="nav-header">Pièces</li>';
        foreach($this->listepiece as $piece ){
            $page .= '<li>
				<a href="'.$this->HttpRequest->getRoute().'/piece?id='.$piece->id.'">'.$piece->nom.'</a>			
            </li>';
        }
        $page .= '</li></ul></div>';
        return $page;


    }






}