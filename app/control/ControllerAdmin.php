<?php
namespace app\control;
use picof\AbstractController;
use \app\vue\Vue;
use \app\model\Piece;
use \app\model\Item;
use \app\model\Type;

class ControllerAdmin extends Controller {

    public function ajouterItem(){
        $vue = new VueBlog();
        if(isset($_SESSION['username'])){
            if(Authentification::checkAccessRights(2)){
                $vue->render(12);
            }else
                $vue->render(15);
        }else
            $vue->render(16);
    }

    public function confirmerAjoutItem(){
        $vue = new VueBlog();
        if(isset($_SESSION['username'])){
            $u = User::where("name","=",$_SESSION['username'])->get()[0];
            $item = new Item();
            $n = filter_var($_POST['nom'],FILTER_SANITIZE_STRING);
            $c = filter_var($_POST['couleur'],FILTER_SANITIZE_STRING);
            $t = filter_var($_POST['type'],FILTER_SANITIZE_STRING);
            $d = filter_var($_POST['description'],FILTER_SANITIZE_STRING);
            $pi = filter_var($_POST['piece'],FILTER_SANITIZE_STRING);
            $p = filter_var($_POST['prix'],FILTER_SANITIZE_STRING);
            $image = move_uploaded_file($_FILES['image']['tmp_name'],'web/image/'.$t);
            if($image){
                $item->photo = true ;
            }else {
                $item->photo = false ;
            }
            $item->nom = $n;
            $item->description = $d;
            $item->type_id = $t->id;
            $item->prix = $p;
            $item->piece_id = $pi->id;
            $item->couleur = $c;

            $item->save();

            $vue->render(1);

        }else{

            $vue->render(16);
        }
    }

    public function ajouterPiece(){
        $vue = new VueBlog();
        if(isset($_SESSION['username'])){
            if(Authentification::checkAccessRights(2)){
                $vue->render(13);
            }else
                $vue->render(15);
        }else
            $vue->render(16);
    }

    public function confirmerAjoutPiece(){
        $vue = new VueBlog();
        if(isset($_SESSION['username'])){
            $u = User::where("name","=",$_SESSION['username'])->get()[0];
            $piece = new Piece();
            $n = filter_var($_POST['nom'],FILTER_SANITIZE_STRING);
            $d = filter_var($_POST['description'],FILTER_SANITIZE_STRING);
            $piece->nom = $n;
            $piece->description = $d;

            $piece->save();
            $vue->render(1);

        }else{

            $vue->render(16);
        }
    }

    public function ajouterType(){
        $vue = new VueBlog();
        if(isset($_SESSION['username'])){
            if(Authentification::checkAccessRights(2)){
                $vue->render(14);
            }else
                $vue->render(15);
        }else
            $vue->render(16);
    }

    public function confirmerAjoutType(){
        $vue = new VueBlog();
        if(isset($_SESSION['username'])){
            $u = User::where("name","=",$_SESSION['username'])->get()[0];
            $type = new Type();
            $t =  filter_var($_POST['type'],FILTER_SANITIZE_STRING);

            $type->type = $t;

            $type->save();
            $vue->render(1);

        }else{

            $vue->render(16);
        }
    }
}