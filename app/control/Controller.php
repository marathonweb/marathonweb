<?php
namespace app\control;
use picof\AbstractController;
use picof\utils\Authentification;
use \app\vue\Vue;
use \app\model\Piece;
use \app\model\Item;
use \app\model\User;
use \app\model\Type;

class Controller {

    public function test(){
        session_start();
        session_start();
        $vue = new Vue();
        $vue->render(0);
    }

    public function accueil(){
        session_start();
        $vue = new Vue();
        $listeitem = Item::all();
        $listepiece = Piece::all();
        $listetype = Type::all();
        $vue->setListeItem($listeitem);
        $vue->setPieces($listepiece);
        $vue->setTypes($listetype);
        $vue->render(1);
    }
    
    public function profil(){
        session_start();
        $vue = new Vue();
        $user = filter_var($_POST['username'],FILTER_SANITIZE_STRING);
        $pass = filter_var($_POST['password'],FILTER_SANITIZE_STRING);
        if(Authentification::authenticate($user,$pass)){
            $id = User::where("name","=",$user)->get();
            $u = $id[0];
            $uid = $u->id;
            Authentification::loadProfile($uid);
        }
        $listeitem = Item::all();
        $listepiece = Piece::all();
        $listetype = Type::all();
        $vue->setListeItem($listeitem);
        $vue->setPieces($listepiece);
        $vue->setTypes($listetype);
        $vue->render(1);
    }

    /*public function afficherItemDetail($iditem){
        $vue = new Vue();
        $id = $this->http->get["iditem"];
        $item = Item::find($iditem);
        $vue->setItem($item);
        $vue->render(2);
    }*/

    public function afficherItem(){
        $vue = new Vue();
        $listeitem = Item::all();
        $listepiece = Piece::all();
        $listetype = Type::all();
        $vue->setListeItem($listeitem);
        $vue->setPieces($listepiece);
        $vue->setTypes($listetype);
        $vue->render(3);
    }

    public function afficherPiece(){
        session_start();
        $vue = new Vue();
        $idpiece = $this->http->get["idpiece"];
        $piece = Piece::find($idpiece);
        $listeitem = $piece->items();
        $listepiece = Piece::all();
        $listetype = Type::all();
        $vue->setPiece($piece);
        $vue->setListeItem($listeitem);
        $vue->setPieces($listepiece);
        $vue->setTypes($listetype);
        $vue->render(5);
    }

    /*public function afficherType($idtype){
        $vue = new Vue();
        $id = $this->http->get["idtype"];
        $type = Type::find($idtype);
        $listeitem = $type->items();
        $listepiece = Piece::all();
        $listetype = Type::all();
        $vue->setListeItem($listeitem);
        $vue->setPieces($listepiece);
        $vue->setTypes($listetype);
        $vue->render(6);
    }

    public function afficherCouleur($couleur){
        $vue = new Vue();
        $listeitem = Item::where('couleur', '=', $couleur)->get();
        $listepiece = Piece::all();
        $listetype = Type::all();
        $vue->setListeItem($listeitem);
        $vue->setPieces($listepiece);
        $vue->setTypes($listetype);
        $vue->render(7);
    }*/

    public function filtre(){
        session_start();
        $vue = new Vue();

        $id_piece = $_POST['piece'];
        $id_type = $_POST['type'];
        $couleur = $_POST['couleur'];

        if ( !is_numeric($id_piece)){
            if ( !is_numeric($id_type)){
                if ( $couleur === "-"){
                    $listeitem = Item::all();
                }else{
                    $listeitem = Item::where('couleur','=',$couleur)->get();
                }
            }else{
                if ( $couleur === "-"){
                    $listeitem = Item::where('type_id','=',$id_type)->get();
                }else{
                    $listeitem = Item::where('type_id','=',$id_type)->where('couleur','=',$couleur)->get();
                }
            }
        }else{
            if ( !is_numeric($id_type)){
                if ( $couleur === "-"){
                    $listeitem = Item::where('piece_id','=',$id_piece)->get();
                }else{
                    $listeitem = Item::where('piece_id','=',$id_piece)->where('couleur','=',$couleur)->get();
                }
            }else{
                if ( $couleur === "-"){
                    $listeitem = Item::where('piece_id','=',$id_piece)->where('type_id','=',$id_type)->get();
                }else{
                    $listeitem = Item::where('piece_id','=',$id_piece)->where('type_id','=',$id_type)->where('couleur','=',$couleur)->get();
                }
            }
        }

        $listepiece = Piece::all();
        $listetype = Type::all();
        $vue->setListeItem($listeitem);
        $vue->setPieces($listepiece);
        $vue->setTypes($listetype);
        $vue->render(3);
    }

    public function top(){
        $vue = new Vue();
        $listetop = Top::all();
        $listepiece = Piece::all();
        $listetype = Type::all();
        $vue->setListeItem($listetop);
        $vue->setPieces($listepiece);
        $vue->setTypes($listetype);
        $vue->render(4);
    }
    
    public function connexion(){
        session_start();
        $vue = new Vue();
        if(isset($_SESSION['username'])){
            $vue->render(9);
        }else
            $vue->render(8);
    }
    
    public function inscription(){
        session_start();
        $vue = new Vue();
        if(isset($_SESSION['username'])){
            $vue->render(9);
        }else
            $vue->render(10);
    }
    
    public function aime(){
        session_start();
        $vue = new Vue();
        if(isset($_SESSION['username'])){
            if(Authentification::checkAccessRights(1)){
                $iditem = filter_var($_POST['id'],FILTER_SANITIZE_NUMBER_INT);
                $item = Item::find($iditem);
                $tmp = $item->aime;
                $item->aime = $tmp+1;
                $item->save();
            }
        }
        $listeitem = Item::all();
        $listepiece = Piece::all();
        $listetype = Type::all();
        $vue->setListeItem($listeitem);
        $vue->setPieces($listepiece);
        $vue->setTypes($listetype);
        $vue->render(3);
    }
    
    public function deconnexion(){
        session_start();
        Authentification::disconnect();
        $vue = new Vue();
        $listeitem = Item::all();
        $listepiece = Piece::all();
        $listetype = Type::all();
        $vue->setListeItem($listeitem);
        $vue->setPieces($listepiece);
        $vue->setTypes($listetype);
        $vue->render(3);
    }

}